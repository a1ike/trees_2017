<?
$keywords = array(
	"giroskutery-optom",
	"kupit-giroskutery-optom",
	"giroskutery-optom-ot-proizvoditelya",
	"giroskutery-optom-v-moskve",
	"postavshchiki-giroskuterov",
	"kupit-giroskutery-optom-v-moskve",
	"giroskuter-smart-optom",
	"giroskuter-smart-balance-optom",
	"giroskutery-optom-s-garantiej"
);
$i=0;
?>

    <!DOCTYPE html>
    <html lang="ru">

    <head>
        <meta charset="utf-8">
        <meta name="description" content="Искусственные ёлки оптом от производителя, в наличии в Москве! Бесплатная франшиза! Купить искусственные ёлки оптом от 120 руб. ">
        <meta name="keywords" content="Купить искусственные елки оптом, куплю искусственные елки оптом, продажа искусственные елки оптом, продажа новогодних елки оптом, искусственные елки цена оптом, искусственные елки оптом Москва, искусственные елки оптом по РФ, искусственные елки оптом производитель, куплю искусственные елки оптом Москва, где купить искусственные елки оптом, покупка искусственные елки оптом, закупка искусственные елки оптом, оптовый склад искусственных елок, оптовая продажа искусственных елок, искусственные елки оптом от производителя, ёлки оптом, искусственные ёлки, искусственные ёлки купить оптом, искусственные ели оптом, искусственные сосны оптом, ели оптом, искусственные ели">
        <meta name="title" content="Оптовая продажа искусственных ёлок">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="yandex-verification" content="baf9d1ebf8e34163" />
        <meta name="google-site-verification" content="COO5NAV4wGsg4nJgMpcblIdJEeufdXH_N4WbiZiLUU8" />

        <title>Искусственные ёлки оптом от производителя</title>

        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
        <link rel="icon" href="/favicon.ico" type="image/x-icon">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" integrity="sha256-jySGIHdxeqZZvJ9SHgPNjbsBP8roij7/WjgkoGTJICk=" crossorigin="anonymous" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css" integrity="sha256-WmhCJ8Hu9ZnPRdh14PkGpz4PskespJwN5wwaFOfvgY8=" crossorigin="anonymous" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.theme.default.min.css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.carousel.min.css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.css" integrity="sha256-UpdOHyyfsvF5Uu6BhbsYQHd1aCNIvxhICDFjz4QbENo=" crossorigin="anonymous" />



        <link rel="stylesheet" href="styles/main.css">

    </head>

    <body>

        <!--[if IE]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

        <header class="el-header">
            <div class="container">
                <div class="el-header__logo">
                    <div class="el-header__logo-title">Твоя-ёлка.рф</div>
                    <h1>
                        Искусственные елки оптом
                    </h1>
                </div>
                <nav class="el-header__nav">
                    <a href="#s1">Каталог ёлок</a>
                    <a href="#s2">Фото</a>
                    <a href="#s3">Отзывы партнеров </a>
                    <a href="#s4">Сотрудничество</a>
                    <a href="#"><strong>Скачать прайс</strong></a>
                </nav>
                <div class="el-header__row">
                    <div class="el-header__row-line">
                        <div>
                            <img src="./images/icon_phone.png" alt="Телефон">
                        </div>
                        <div>
                            <a href="tel:+74991112233"><strong>8 (499) 111-22-33</strong></a><br>
                        </div>
                    </div>
                    <div class="el-header__row-line">
                        <div>
                            <img src="./images/icon_phone.png" alt="Телефон">
                        </div>
                        <div>
                            <a href="tel:+79670651122">8 (967) 065-11-22</a>
                            <a href="#" class="el-header__small-social">
                                <img src="./images/icon_wu.png" alt="WhatsUp">
                            </a>
                            <a href="#" class="el-header__small-social">
                                <img src="./images/icon_tg.png" alt="Telegram">
                            </a>
                            <a href="#" class="el-header__small-social">
                                <img src="./images/icon_vb.png" alt="Viber">
                            </a>

                        </div>

                    </div>
                    <div class="el-header__row-line">
                        <p>
                            Шоу-рум: Новый Арбат 21<br> Ежедневно с 10:00 до 20:00
                        </p>
                    </div>
                </div>
            </div>
        </header>

        <div class="container">
            <div class="el-hello">
                <div class="activity res">
                    <h2 class="din_bold">Искусственные ёлки оптом купить от 120 руб</h2><br>
                    <span class="diin_bold"><span class="yellow">закупка от 15 т.р. |  доходность до 300%  |  бесплатная франшиза  |  доставка за 2 дня </span></span>
                </div>
            </div>
        </div>

        <div class="form_wrap">
            <div class="count_down">
                <div class="first_line clear_self">
                    <div class="num big_noodle">- 11%</div>
                    <div class="din_bold">СКИДКА НА ПРЕДЗАКАЗ!</div> СПЕЦПРЕДЛОЖЕНИЕ ОГРАНИЧЕНО
                </div>
                <div class="counter clear_self">
                    <div class="desc">ДО КОНЦА<br>АКЦИИ:</div>
                    <div data-year="2016" data-month="12" data-day="22" class="this-box5-timer" id="counter1"></div>
                </div>
            </div>
            <div class="form_bg bbox">
                <div class="name din_medium">
                    СКАЧАЙТЕ ОПТОВЫЙ ПРАЙС <br> и получите скидку на предзаказ
                </div>
                <form action="form-ok.php" method="post" class="this_is_my_lead_form">
                    <input type="text" name="avtor" placeholder="Имя" class="bbox picted name name_lead_telegram">
                    <input type="text" name="tel" placeholder="Телефон" class="bbox picted tel phone_lead_telegram">
                    <input type="text" name="email" placeholder="e-mail" class="bbox picted mail email_lead_telegram">
                    <input type="submit" value="СКАЧАТЬ" class="but bbox din_bold" onClick="yaCounter40640220.reachGoal('ORDER1');">
                </form>
                <div class="note din">Мы гарантируем безопасность Ваших персональных данных.</div>
            </div>
        </div>

        <!--<div class="container">
        <div class="el-companies">

            <img src="./images/company_1.png" alt="Название компании">
            <img src="./images/company_2.png" alt="Название компании">
            <img src="./images/company_3.png" alt="Название компании">
            <img src="./images/company_1.png" alt="Название компании">
            <img src="./images/company_2.png" alt="Название компании">
            <img src="./images/company_3.png" alt="Название компании">

        </div>
    </div> -->

        <div class="container">
            <div class="el-why">

                <div class="el-why__card el-why__card_1">
                    <img src="./images/why_1.png" alt="5 лет на рынке">
                </div>

                <div class="el-why__card el-why__card_2">
                    <img src="./images/why_2.png" alt="Поставки по России и СНГ">
                </div>

                <div class="el-why__card el-why__card_3">
                    <img src="./images/why_3.png" alt="Обеспечиваем ёлками лидеров розничного рынка">
                </div>

            </div>
        </div>

        <div class="el-price">
            <div class="container">
                <h3 class="el-price__title"><span>Бесплатная франшиза</span> искусственных ёлок</h3>
                <p class="el-price__sub-title">
                    Мастера - профессионалы в своей области - регулярно проходят курсы повышения квалификации и аттестацию. Все они - постоянные сотрудники.
                </p>
                <div class="el-price__cards">

                    <section class="el-price__card">
                        <header class="el-price__card-header">
                            <p>при закупке</p>
                            <div class="el-price__card-price">от 155 000 руб</div>
                            <p>получаете:</p>
                        </header>
                        <div>
                            <div class="el-price__card-feature">
                                Все заявки по вашему региону

                            </div>
                            <div class="el-price__card-feature">
                                Оптовую скидку до 64%

                            </div>
                            <div class="el-price__card-feature">
                                Сайт под ключ
                            </div>
                            <div class="el-price__card-feature">
                                Рекламные компании Яндекс Директ + Google Adwords
                            </div>

                            <div class="el-price__card-feature">
                                Руководство по продажам на Avito
                            </div>
                            <div class="el-price__card-feature">
                                Руководство по продажам в соц сетях
                            </div>
                            <div class="el-price__card-feature">
                                Скрипты продаж
                            </div>
                            <div class="el-price__card-feature">
                                Автоматизация отдела продаж, склада и доставки
                            </div>
                            <div class="el-price__card-feature">
                                Поддержка 24/7
                                <div class="el-price__card-info">
                                    (личный менеджер, доступ в закрытый чат, обучающие вебинары)
                                </div>
                            </div>
                            <div class="el-price__card-feature">
                                Промо-материалы
                                <ul>
                                    <li>Дизайн гр. VK и Instagram</li>
                                    <li>Макеты листовок (флаера/расклейка)</li>
                                    <li>Макеты продающих ценников</li>
                                    <li>Шаблон объявлния для размещения на досках</li>
                                    <li>Набор фото и видео</li>
                                    <li>Шаблон рекламы на радио</li>
                                </ul>
                            </div>
                        </div>
                    </section>

                    <section class="el-price__card">
                        <header class="el-price__card-header">
                            <p>при закупке</p>
                            <div class="el-price__card-price">от 55 000 руб</div>
                            <p>получаете:</p>
                        </header>
                        <div>
                            <div class="el-price__card-feature">
                                Оптовый прайс-лист
                            </div>
                            <div class="el-price__card-feature">
                                Бонусы
                            </div>
                            <div class="el-price__card-feature">
                                Макеты листовок
                            </div>
                            <div class="el-price__card-feature">
                                Шаблон базы
                            </div>
                            <div class="el-price__card-feature">
                                Промо-материалы
                                <ul>
                                    <li>Лендинг-пэйдж</li>
                                    <li>Макеты листовок (раздатка/расклейка)</li>
                                    <li>Готовый таргетинг</li>
                                </ul>
                            </div>
                        </div>
                    </section>

                    <section class="el-price__card">
                        <header class="el-price__card-header">
                            <p>при закупке</p>
                            <div class="el-price__card-price">от 15 000 руб</div>
                            <p>получаете:</p>
                        </header>
                        <div>
                            <div class="el-price__card-feature">
                                Оптовый прайс-лист
                            </div>
                            <div class="el-price__card-feature">
                                Бонусы
                            </div>
                            <div class="el-price__card-feature">
                                Макеты листовок
                            </div>
                            <div class="el-price__card-feature">
                                Шаблон базы
                            </div>
                        </div>
                    </section>

                </div>
                <a href="#" class="el-price__download">
                    <img src="./images/icon_xls.png" alt="Скачать">
                    <span>скачать прайс-лист</span>
                </a>
            </div>
        </div>

        <div class="el-model">
            <div class="container">
                <div class="el-model__title">Готовая <span>бизнес модель</span></div>
                <p class="el-price__sub-title">
                    Бесплатно предоставляем полностью готовый бизнес план по продаже искусственных ёлок. <br>С помощью которого, вы запустите бизнес по продаже искусственных ёлок за 2 дня.
                </p>
                <div class="el-model__cards">

                    <div class="el-model__card">
                        <img src="./images/model_1.png" alt="План">
                        <div class="el-model__card-title">Бизнес план</div>
                    </div>

                    <div class="el-model__card">
                        <img src="./images/model_2.png" alt="План">
                        <div class="el-model__card-title">Привлечение клиентов</div>
                    </div>

                    <div class="el-model__card">
                        <img src="./images/model_3.png" alt="План">
                        <div class="el-model__card-title">Работа с персоналом</div>
                    </div>

                    <div class="el-model__card">
                        <img src="./images/model_4.png" alt="План">
                        <div class="el-model__card-title">Организация доставки</div>
                    </div>

                    <div class="el-model__card">
                        <img src="./images/model_5.png" alt="План">
                        <div class="el-model__card-title">Работа с отдела продаж</div>
                    </div>

                    <div class="el-model__card">
                        <img src="./images/model_6.png" alt="План">
                        <div class="el-model__card-title">Работа склада и бухгалтерия</div>
                    </div>

                    <div class="el-model__card">
                        <img src="./images/model_7.png" alt="План">
                        <div class="el-model__card-title">Маркетинговые материалы</div>
                    </div>

                    <div class="el-model__card">
                        <img src="./images/model_8.png" alt="План">
                        <div class="el-model__card-title">Шаблоны, макеты, дизайн</div>

                    </div>

                </div>
            </div>
        </div>

        <div id="s1" class="el-goods">
            <div class="container">
                <h4 class="el-goods__title"><span>Искуственные Ёлки<br></span> оптом в Москве </h4>
                <p class="el-goods__sub-title">
                    Купить искусственные елки оптом дешево в Москве от поставщика, можно в нашем магазине. Мы подобрали самые продаваемые позиции ёлок и сопутствующих товаров для до-продажи.
                </p>
                <div id="el-tab" class="el-goods__tabs">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#1" data-toggle="tab">Ёлки</a></li>
                        <li><a href="#2" data-toggle="tab">Гирлянды</a></li>
                        <li><a href="#3" data-toggle="tab">Мишура</a></li>
                        <li><a href="#4" data-toggle="tab">Игрушки</a></li>
                    </ul>
                    <div class="tab-content">

                        <div class="tab-pane active" id="1">
                            <div class="el-goods__cards">

                                <div class="el-goods__card">
                                    <div class="el-goods__card-left">
                                        <div class="el-goods__card-tree">
                                            <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            <div class="el-goods__card-zoom"></div>
                                        </div>
                                        <div class="el-goods__card-slick">
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="el-goods__card-right">
                                        <div class="el-goods__card-title">
                                            Ёлочка 60 см
                                        </div>
                                        <div class="el-goods__card-description">
                                            <p>
                                                Высота: 60 см<br> Диаметр: 35 см<br> Кол-во веток: 24 шт<br> Вес: 0,4 кг<br> Размер упаковки: 50см х 4см х 4см
                                            </p>
                                            <div class="el-goods__card-price">
                                                Цена: от 120 руб.
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="el-goods__card">
                                    <div class="el-goods__card-left">
                                        <div class="el-goods__card-tree">
                                            <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            <div class="el-goods__card-zoom"></div>
                                        </div>
                                        <div class="el-goods__card-slick">
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="el-goods__card-right">
                                        <div class="el-goods__card-title">
                                            Ёлочка 90 см
                                        </div>
                                        <div class="el-goods__card-description">
                                            <p>
                                                Высота: 90 см<br> Диаметр: 60 см<br> Кол-во веток: 38 шт<br> Вес: 0,7 кг<br> Размер упаковки: 80см х 6см х 6см
                                            </p>
                                            <div class="el-goods__card-price">
                                                Цена: от 140 руб.
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="el-goods__card">
                                    <div class="el-goods__card-left">
                                        <div class="el-goods__card-tree">
                                            <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            <div class="el-goods__card-zoom"></div>
                                        </div>
                                        <div class="el-goods__card-slick">
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="el-goods__card-right">
                                        <div class="el-goods__card-title">
                                            Ёлочка 120 см
                                        </div>
                                        <div class="el-goods__card-description">
                                            <p>
                                                Высота: 120 см<br> Диаметр: 78 см<br> Кол-во веток: 102 шт<br> Вес: 2,5 кг<br> Размер упаковки: 60см х 15см х 14см
                                            </p>
                                            <div class="el-goods__card-price">
                                                Цена: от 111 руб.
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="el-goods__card">
                                    <div class="el-goods__card-left">
                                        <div class="el-goods__card-tree">
                                            <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            <div class="el-goods__card-zoom"></div>
                                        </div>
                                        <div class="el-goods__card-slick">
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="el-goods__card-right">
                                        <div class="el-goods__card-title">
                                            Ёлочка 150 см
                                        </div>
                                        <div class="el-goods__card-description">
                                            <p>
                                                Высота: 150 см<br> Диаметр: 100 см<br> Кол-во веток: 142 шт<br> Вес: 3,5 кг<br> Размер упаковки: 72см х 18см х 15см
                                            </p>
                                            <div class="el-goods__card-price">
                                                Цена: от 111 руб.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="el-goods__card">
                                    <div class="el-goods__card-left">
                                        <div class="el-goods__card-tree">
                                            <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            <div class="el-goods__card-zoom"></div>
                                        </div>
                                        <div class="el-goods__card-slick">
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="el-goods__card-right">
                                        <div class="el-goods__card-title">
                                            Ёлочка 180 см
                                        </div>
                                        <div class="el-goods__card-description">
                                            <p>
                                                Высота: 180 см<br> Диаметр: 125 см<br> Кол-во веток: 194 шт<br> Вес: 4,5 кг<br> Размер упаковки: 72см х 19см х 18см
                                            </p>
                                            <div class="el-goods__card-price">
                                                Цена: от 111 руб.
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="el-goods__card">
                                    <div class="el-goods__card-left">
                                        <div class="el-goods__card-tree">
                                            <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            <div class="el-goods__card-zoom"></div>
                                        </div>
                                        <div class="el-goods__card-slick">
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="el-goods__card-right">
                                        <div class="el-goods__card-title">
                                            Ёлочка 210 см
                                        </div>
                                        <div class="el-goods__card-description">
                                            <p>
                                                Высота: 210 см<br> Диаметр: 145 см<br> Кол-во веток: 264 шт<br> Вес: 6 кг<br> Размер упаковки: 72см х 24см х 20см
                                            </p>
                                            <div class="el-goods__card-price">
                                                Цена: от 111 руб.
                                            </div>
                                        </div>
                                    </div>
                                </div>



                                <div class="el-goods__card">
                                    <div class="el-goods__card-left">
                                        <div class="el-goods__card-tree">
                                            <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            <div class="el-goods__card-zoom"></div>
                                        </div>
                                        <div class="el-goods__card-slick">
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="el-goods__card-right">
                                        <div class="el-goods__card-title">
                                            Ёлочка 240 см
                                        </div>
                                        <div class="el-goods__card-description">
                                            <p>
                                                Высота: 240 см<br> Диаметр: 160 см<br> Кол-во веток: 354 шт<br> Вес: 7,5 кг<br> Размер упаковки: 75см х 28см х 25см
                                            </p>
                                            <div class="el-goods__card-price">
                                                Цена: от 111 руб.
                                            </div>
                                        </div>
                                    </div>
                                </div>









                            </div>
                        </div>

                        <div class="tab-pane" id="2">
                            <div class="el-goods__cards">

                                <div class="el-goods__card">
                                    <div class="el-goods__card-left">
                                        <div class="el-goods__card-tree">
                                            <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            <div class="el-goods__card-zoom"></div>
                                        </div>
                                        <div class="el-goods__card-slick">
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="el-goods__card-right">
                                        <div class="el-goods__card-title">
                                            Ёлочка 180 см
                                        </div>
                                        <div class="el-goods__card-description">
                                            <p>
                                                Высота ёлки: 180 см<br> Тип елки: Искусственные елки<br> Длина хвои ветки: 0.45 см<br> Защита от огня: нет<br> Количество веток: шт
                                            </p>
                                            <div class="el-goods__card-price">
                                                Цена: от 111 руб.
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="el-goods__card">
                                    <div class="el-goods__card-left">
                                        <div class="el-goods__card-tree">
                                            <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            <div class="el-goods__card-zoom"></div>
                                        </div>
                                        <div class="el-goods__card-slick">
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="el-goods__card-right">
                                        <div class="el-goods__card-title">
                                            Ёлочка 210 см
                                        </div>
                                        <div class="el-goods__card-description">
                                            <p>
                                                Высота ёлки: 210 см<br> Тип елки: Искусственные елки<br> Длина хвои ветки: 0.45 см<br> Защита от огня: нет<br> Количество веток: шт
                                            </p>
                                            <div class="el-goods__card-price">
                                                Цена: от 111 руб.
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="tab-pane" id="3">
                            <div class="el-goods__cards">

                                <div class="el-goods__card">
                                    <div class="el-goods__card-left">
                                        <div class="el-goods__card-tree">
                                            <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            <div class="el-goods__card-zoom"></div>
                                        </div>
                                        <div class="el-goods__card-slick">
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="el-goods__card-right">
                                        <div class="el-goods__card-title">
                                            Ёлочка 240 см
                                        </div>
                                        <div class="el-goods__card-description">
                                            <p>
                                                Высота ёлки: 50 см<br> Тип елки: Искусственные елки<br> Длина хвои ветки: 0.45 см<br> Защита от огня: нет<br> Количество веток: шт
                                            </p>
                                            <div class="el-goods__card-price">
                                                Цена: от 111 руб.
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="el-goods__card">
                                    <div class="el-goods__card-left">
                                        <div class="el-goods__card-tree">
                                            <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            <div class="el-goods__card-zoom"></div>
                                        </div>
                                        <div class="el-goods__card-slick">
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="el-goods__card-right">
                                        <div class="el-goods__card-title">
                                            Ёлочка 50 см
                                        </div>
                                        <div class="el-goods__card-description">
                                            <p>
                                                Высота ёлки: 50 см<br> Тип елки: Искусственные елки<br> Длина хвои ветки: 0.45 см<br> Защита от огня: нет<br> Количество веток: шт
                                            </p>
                                            <div class="el-goods__card-price">
                                                Цена: от 111 руб.
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="el-nice">

            <div class="elka" style="background-color: rgb(243, 243, 243); width: 100%; margin-bottom: -80px; padding-bottom: 80px; padding-top: 24px; position: relative; text-align: center; display: inline-block;">
                <h5 class="el-title_h5">ИСКУСТВЕННЫЕ ЕЛКИ ОТ ПРОИЗВОДИТЕЛЯ<br><span class="din_bold">С ГАРАНТИЕЙ КАЧЕСТВА</span></h5>
                <div style="position: relative; display: inline-block; text-align: center; width: 980px; float: none;" class="content">
                    <img alt="искусственные елки оптом" src="./images/why1.png">
                    <div style="left: -95px; top: 50px;" class="zagotext">
                        <h3>ПЫШНЫЕ ВЕТКИ</h3>
                        <p>За счет роскошной пышности ёлочка выглядит очень эффектно. Длина иголок 5-7 см.</p>
                    </div>
                    <div style="top: 50px; left: 870px;" class="zagotext">
                        <h3>ЖИВЫЕ ШИШКИ</h3>
                        <p>В комплекте с ёлочкой прилагаются живые шишки для более эффектного вида будущей ёлочки</p>
                    </div>
                    <div style="left: -80px; top: 535px;" class="zagotext">
                        <h3>ЗАСНЕЖЕННЫЕ ВЕТКИ</h3>
                        <p>Пышные ветки с снежным напылением делают ёлочку неотразимой.</p>
                    </div>
                    <div style="left: 850px; top: 519px;" class="zagotext">
                        <h3>ПРОЧНЫЕ КРЕПЛЕНИЯ</h3>
                        <p>Елка имеет отличную устойчивость за счет множества прочных креплений. Ее легко собирать\разбирать и хранить</p>
                    </div>
                    <div style="left: -50px; top: 778px;" class="zagotext">
                        <h3>УСТОЙЧИВАЯ ПОДСТАВКА</h3>
                        <p>Ёлочки имеют устойчивое металлическое основание. Она не упадет под весом игрушек</p>
                    </div>
                    <div style="left: 611px; top: 780px;" class="zagotext">
                        <h3>ДОЛГОВЕЧНА И ЭКОНОЛОГИЧНА</h3>
                        <p>Ёлочка производится из качественных и экологически чистых материалов, которые прошли контроль качества в РФ.</p>
                    </div>
                </div>
            </div>
        </div>

        <div id="s3" class="el-reviews">
            <div class="container">
                <div class="el-reviews__title"><span>Отзывы</span> наших партнёров</div>
                <p class="el-reviews__sub-title">
                    Мастера - профессионалы в своей области - регулярно проходят курсы повышения квалификации и аттестацию. Все они - постоянные сотрудники.
                </p>
                <div class="el-reviews__cards">

                    <div class="el-reviews__card">
                        <div class="el-reviews__card-left">
                            <img src="./images/man.png" class="el-reviews__card-photo">
                            <a data-fancybox="reviews" href="https://www.youtube.com/embed/mEErTZTHBuY">
                                <img src="./images/youtube.png" alt="Видео">
                                <p class="el-reviews__card-show">Смотреть видео <img src="./images/icon_show.png" alt="Смотреть видео"></p>
                            </a>
                        </div>
                        <div class="el-reviews__card-right">
                            <div class="el-reviews__card-title">
                                <strong>Сеть розничных магазинов</strong> Прибыль за 1.5мес: 591 000руб
                            </div>
                            <p>
                                Замеры объёма продаж всех представленных искусственных сосен, проводимые аудиторской комиссией нашей сети магазинов, показали, что искусственные сосны поставщика "Твоя-ёлка" продаются в 1,7 раза лучше других поставщиков уже на протяжении последних 2-х
                                лет!
                            </p>
                            <p>
                                От себя хочу добавить: весь процессы закупок и поставок проходит гладко и без нервов, за что очень Вам признателен!
                            </p>
                            <div class="el-reviews__card-from">
                                <strong>Алексей,</strong> г. Екатеринбург
                            </div>
                        </div>
                    </div>

                    <div class="el-reviews__card">
                        <div class="el-reviews__card-left">
                            <img src="./images/man.png" class="el-reviews__card-photo">
                            <a data-fancybox="reviews" href="./images/paper.png">
                                <img src="./images/paper.png" alt="Сертификат">
                                <p class="el-reviews__card-show">Благодарность <img src="./images/icon_show.png" alt="Смотреть видео"></p>
                            </a>
                        </div>
                        <div class="el-reviews__card-right">
                            <div class="el-reviews__card-title">
                                <strong>Сеть розничных магазинов</strong> Прибыль за 1.5мес: 591 000руб
                            </div>
                            <p>
                                Замеры объёма продаж всех представленных искусственных сосен, проводимые аудиторской комиссией нашей сети магазинов, показали, что искусственные сосны поставщика "Твоя-ёлка" продаются в 1,7 раза лучше других поставщиков уже на протяжении последних 2-х
                                лет!
                            </p>
                            <p>
                                От себя хочу добавить: весь процессы закупок и поставок проходит гладко и без нервов, за что очень Вам признателен!
                            </p>
                            <div class="el-reviews__card-from">
                                <strong>Алексей,</strong> г. Екатеринбург
                            </div>
                        </div>
                    </div>

                    <div class="el-reviews__card">
                        <div class="el-reviews__card-left">
                            <img src="./images/man.png" class="el-reviews__card-photo">
                            <a data-fancybox="reviews" href="https://www.youtube.com/embed/mEErTZTHBuY">
                                <img src="./images/youtube.png" alt="Видео">
                                <p class="el-reviews__card-show">Смотреть видео <img src="./images/icon_show.png" alt="Смотреть видео"></p>
                            </a>
                        </div>
                        <div class="el-reviews__card-right">
                            <div class="el-reviews__card-title">
                                <strong>Сеть розничных магазинов</strong> Прибыль за 1.5мес: 591 000руб
                            </div>
                            <p>
                                Замеры объёма продаж всех представленных искусственных сосен, проводимые аудиторской комиссией нашей сети магазинов, показали, что искусственные сосны поставщика "Твоя-ёлка" продаются в 1,7 раза лучше других поставщиков уже на протяжении последних 2-х
                                лет!
                            </p>
                            <p>
                                От себя хочу добавить: весь процессы закупок и поставок проходит гладко и без нервов, за что очень Вам признателен!
                            </p>
                            <div class="el-reviews__card-from">
                                <strong>Алексей,</strong> г. Екатеринбург
                            </div>
                        </div>
                    </div>

                    <div class="el-reviews__card">
                        <div class="el-reviews__card-left">
                            <img src="./images/man.png" class="el-reviews__card-photo">
                            <a data-fancybox="reviews" href="./images/paper.png">
                                <img src="./images/paper.png" alt="Сертификат">
                                <p class="el-reviews__card-show">Благодарность <img src="./images/icon_show.png" alt="Смотреть видео"></p>
                            </a>
                        </div>
                        <div class="el-reviews__card-right">
                            <div class="el-reviews__card-title">
                                <strong>Сеть розничных магазинов</strong> Прибыль за 1.5мес: 591 000руб
                            </div>
                            <p>
                                Замеры объёма продаж всех представленных искусственных сосен, проводимые аудиторской комиссией нашей сети магазинов, показали, что искусственные сосны поставщика "Твоя-ёлка" продаются в 1,7 раза лучше других поставщиков уже на протяжении последних 2-х
                                лет!
                            </p>
                            <p>
                                От себя хочу добавить: весь процессы закупок и поставок проходит гладко и без нервов, за что очень Вам признателен!
                            </p>
                            <div class="el-reviews__card-from">
                                <strong>Алексей,</strong> г. Екатеринбург
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="container">
            <div class="el-friends">
                <div class="clients res">
                    <h1>С КЕМ МЫ <span class="din_bold">ХОТИМ СОТРУДНИЧАТЬ</span></h1>
                    <li>
                        <a href="#a"><img src="images/partner1.png" alt=""></a>
                    </li>
                    <li>
                        <a href="#a"><img src="images/partner3.png" alt=""></a>
                    </li>
                    <li>
                        <a href="#a"><img src="images/partner5.png" alt=""></a>
                    </li>
                    <li>
                        <a href="#a"><img src="images/partner2.png" alt=""></a>
                    </li>
                </div>
            </div>
        </div>

        <div class="form_wrap">
            <div class="count_down">
                <div class="first_line clear_self">
                    <div class="num big_noodle">- 35%</div>
                    <div class="din_bold">СКИДКА НА ПРЕДЗАКАЗ!</div> СПЕЦПРЕДЛОЖЕНИЕ ОГРАНИЧЕНО

                </div>
                <div class="counter clear_self">
                    <div class="desc">ДО КОНЦА<br>АКЦИИ:</div>
                    <div data-year="2016" data-month="12" data-day="22" class="this-box5-timer" id="counter2"></div>
                </div>
            </div>
            <div class="form_bg bbox">
                <div class="name din_medium">
                    СКАЧАЙТЕ ОПТОВЫЙ ПРАЙС <br> и получите скидку на предзаказ
                </div>
                <form action="form-ok.php" method="post" class="this_is_my_lead_form">
                    <input type="text" name="avtor" placeholder="Имя" class="bbox picted name name_lead_telegram">
                    <input type="text" name="tel" placeholder="Телефон" class="bbox picted tel phone_lead_telegram">
                    <input type="text" name="email" placeholder="e-mail" class="bbox picted mail email_lead_telegram">
                    <input type="submit" value="СКАЧАТЬ" class="but bbox din_bold" onClick="yaCounter40640220.reachGoal('ORDER1');">
                </form>
                <div class="note din">Мы гарантируем безопасность Ваших персональных данных.</div>
            </div>
        </div>

        <div id="s4" class="el-profit">
            <div class="container">
                <div class="el-profit__title"><span>ТВОЯ-ЁЛКА.РФ НАДЕЖНЫЙ</span><br> ПОСТАВЩИК ИСКУСТВЕННЫХ ЕЛОК ОПТОМ</div>
                <p class="el-profit__sub-title">
                    Мастера - профессионалы в своей области - регулярно проходят курсы повышения квалификации и аттестацию. Все они - постоянные сотрудники.
                </p>
                <div class="el-profit__cards">

                    <div class="el-profit__card">
                        <img src="./images/friends_1.png" alt="Лучшая цена">
                        <div class="el-profit__card-title">
                            ЛУЧШАЯ ЗАКУПОЧНАЯ ЦЕНА
                        </div>
                        <p>
                            мы поставляем елки напрямую от производителя, покупая елки у нас вы не переплачиваете спекулянтам!
                        </p>
                    </div>

                    <div class="el-profit__card">
                        <img src="./images/friends_2.png" alt="Лучшаяцена">
                        <div class="el-profit__card-title">
                            ПОМОГАЕМ С РЕАЛИЗАЦИЕЙ
                        </div>
                        <p>
                            бесплатно предоставляем маркетинговый пакет инструментов - от сайта до оформления павильона!
                        </p>
                    </div>

                    <div class="el-profit__card">
                        <img src="./images/friends_3.png" alt="Лучшаяцена">
                        <div class="el-profit__card-title">
                            ВЫСОКОЕ КАЧЕСТВО ПРОДУКЦИИ
                        </div>
                        <p>
                            мы поставляем только красивые, пушистые, устойчивые елки с индивидуальной упаковкой каждой ветки!
                        </p>
                    </div>

                    <div class="el-profit__card">
                        <img src="./images/friends_4.png" alt="Лучшаяцена">
                        <div class="el-profit__card-title">
                            БЕСПЕРЕБОЙНЫЕ ПОСТАВКИ
                        </div>
                        <p>
                            постоянные, ежедневные поставки любых объемов, всегда резервируем товар под каждого клиента!
                        </p>
                    </div>

                    <div class="el-profit__card">
                        <img src="./images/friends_5.png" alt="Лучшаяцена">
                        <div class="el-profit__card-title">
                            ЗАВОДСКАЯ ГАРАНТИЯ
                        </div>
                        <p>
                            полный возврат денежных средств за любой брак товара. Процент брака на одно изделие менее 0,1%!
                        </p>
                    </div>

                    <div class="el-profit__card">
                        <img src="./images/friends_6.png" alt="Лучшаяцена">
                        <div class="el-profit__card-title">
                            ПУНКТУАЛЬНАЯ ДОСТАВКА
                        </div>
                        <p>
                            мы экономим ваши деньги и время, используя проверенные и самые дешевые транспортные компании!
                        </p>
                    </div>

                </div>
            </div>
        </div>

        <div class="el-team">
            <div class="container">
                <div class="el-team__title">наша <span>команда</span></div>
                <p class="el-team__sub-title">
                    Мастера - профессионалы в своей области - регулярно проходят курсы повышения квалификации и аттестацию. Все они - постоянные сотрудники.
                </p>
                <div class="row clearfix">
                    <div class="col-md-8">
                        <img src="./images/team.jpg" alt="Наша команда">
                    </div>
                    <div class="col-md-4">
                        <ul>
                            <li>
                                Хорошая компания, она честна перед коллегами и клиентами
                            </li>
                            <li>
                                Хорошая компания, она честна перед коллегами и клиентами
                            </li>
                            <li>
                                Хорошая компания, она честна перед коллегами и клиентами
                            </li>
                            <li>
                                Хорошая компания, она честна перед коллегами и клиентами
                            </li>
                            <li>
                                Хорошая компания, она честна перед коллегами и клиентами
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div id="s2" style="position: relative;" class="container">
            <div class="el-gallery">

                <a data-fancybox="gallery" href="./images/gallery_2.png" href="#" class="el-gallery__card">
                    <img src="./images/gallery_2.png" alt="Галлерея">
                </a>

                <a data-fancybox="gallery" href="./images/gallery_3.png" href="#" class="el-gallery__card">
                    <img src="./images/gallery_3.png" alt="Галлерея">
                </a>

                <a data-fancybox="gallery" href="./images/gallery_2.png" href="#" class="el-gallery__card">
                    <img src="./images/gallery_2.png" alt="Галлерея">
                </a>

                <a data-fancybox="gallery" href="./images/gallery_3.png" href="#" class="el-gallery__card">
                    <img src="./images/gallery_3.png" alt="Галлерея">
                </a>

                <a data-fancybox="gallery" href="./images/gallery_2.png" href="#" class="el-gallery__card">
                    <img src="./images/gallery_2.png" alt="Галлерея">
                </a>

                <a data-fancybox="gallery" href="./images/gallery_3.png" href="#" class="el-gallery__card">
                    <img src="./images/gallery_3.png" alt="Галлерея">
                </a>

                <a data-fancybox="gallery" href="./images/gallery_2.png" href="#" class="el-gallery__card">
                    <img src="./images/gallery_2.png" alt="Галлерея">
                </a>

                <a data-fancybox="gallery" href="./images/gallery_3.png" href="#" class="el-gallery__card">
                    <img src="./images/gallery_3.png" alt="Галлерея">
                </a>

            </div>
        </div>

        <div class="container">
            <div class="el-videos">
                <div class="el-videos__title">Наше производство</div>
                <div class="row">
                    <div class="col-md-6">
                        <iframe width="100%" height="315" src="https://www.youtube.com/embed/-AUE4ZrMItI" frameborder="0" allowfullscreen></iframe>
                    </div>
                    <div class="col-md-6">
                        <iframe width="100%" height="315" src="https://www.youtube.com/embed/-AUE4ZrMItI" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>

        <div class="el-seo">
            <div class="container">
                <div class="el-seo__title">Текст для <span>продвижения</span></div>
                <p class="el-seo__sub-title">
                    Мастера - профессионалы в своей области - регулярно проходят курсы повышения квалификации и аттестацию. Все они - постоянные сотрудники.
                </p>
                <article class="el-seo__article">
                    <p>
                        Ролевое поведение дает непосредственный монтаж. Шиллер утверждал: статус художника изящно имитирует сокращенный модернизм, подобный исследовательский подход к проблемам художественной типологии можно обнаружить у К.Фосслера. Экзистенциализм, в первом
                        приближении, имитирует элитарный структурализм.
                    </p>
                    <p>
                        Механизм эвокации готично продолжает героический миф. Типическое аккумулирует глубокий культовый образ. Аполлоновское начало композиционно. Монтаж, следовательно, вызывает первоначальный комплекс априорной бисексуальности. Комплекс агрессивности, на первый
                        взгляд, органичен. Стиль монотонно дает невротический хтонический миф.Механизм эвокации готично продолжает героический миф. Типическое аккумулирует глубокий культовый образ. Аполлоновское начало композиционно. Монтаж, следовательно,
                        вызывает первоначальный комплекс априорной бисексуальности. Комплекс агрессивности, на первый взгляд, органичен. Стиль монотонно дает невротический хтонический миф.
                    </p>
                </article>
            </div>
        </div>

        <div class="el-footer">
            <iframe src="https://yandex.ru/map-widget/v1/-/CBUhuIHZWC" width="100%" height="610" frameborder="0"></iframe>
            <div class="el-footer__container">
                <h6 class="el-footer__title">ООО «ИСКУСТВЕННЫЕ ЕЛКИ ОПТОМ»</h6>
                <div class="el-footer__row">
                    <div>
                        <img src="./images/icon_place.png" alt="Место">
                    </div>
                    <div>
                        <p>
                            Россия, г. Москва, ул. Смоленский бульвар 17/5
                        </p>
                    </div>
                </div>
                <div class="el-footer__row">
                    <div>
                        <img src="./images/icon_doc.png" alt="Данные">
                    </div>
                    <div>
                        <p>
                            ОГРН 1127747291495; ИП Белый К.Ю. <br> г.Москва ул& Новый Арбат 21, БЦ Обновление Арбата
                        </p>
                    </div>
                </div>
                <div class="el-footer__row">
                    <div>
                        <img src="./images/icon_phone.png" alt="Телефон">
                    </div>
                    <div>
                        <a href="tel:+74991112233"><strong>8 (499) 111-22-33</strong></a><br>
                        <a href="tel:+79670651122">8 (967) 065-11-22</a>
                        <a href="#" class="el-footer__small-social">
                            <img src="./images/icon_wu.png" alt="WhatsUp">
                        </a>
                        <a href="#" class="el-footer__small-social">
                            <img src="./images/icon_tg.png" alt="Telegram">
                        </a>
                        <a href="#" class="el-footer__small-social">
                            <img src="./images/icon_vb.png" alt="Viber">
                        </a>
                    </div>
                </div>
                <div class="el-footer__row">
                    <div>
                        <img src="./images/icon_doc.png" alt="Данные">
                    </div>
                    <div>
                        <a href="#" class="el-footer__price">Скачать прайс для оптовиков</a>
                    </div>
                </div>
                <ul class="el-footer__social">
                    <li>
                        <a href="#"><img src="./images/icon_vk.png" alt="VK"></a>
                    </li>
                    <li>
                        <a href="#"><img src="./images/icon_fb.png" alt="FB"></a>
                    </li>
                    <li>
                        <a href="#"><img src="./images/icon_ig.png" alt="IG"></a>
                    </li>
                </ul>
                <div class="el-footer__gallery">
                    <img src="./images/footer_1.png" alt="Галлерея">
                    <img src="./images/footer_2.png" alt="Галлерея">
                </div><br>
                <a href="#">Политика конфеденциальности</a><br>
                <a href="#">Партнерская программа</a>
            </div>
        </div>

        <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-scrollTo/2.1.0/jquery.scrollTo.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js" integrity="sha256-4Cr335oZDYg4Di3OwgUOyqSTri0jUm2+7Gf2kH3zp1I=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.js" integrity="sha256-MXT+AJD8HdXQ4nLEbqkMqW3wXXfvjaGQt/Q/iRlBNSU=" crossorigin="anonymous"></script>

        <script src="scripts/jquery.countdown.js"></script>
        <script src="scripts/main.js"></script>

        <script type="application/ld+json">
            {
                "@context": "http://schema.org",
                "@type": "Organization",
                "url": "http://www.example.com",
                "logo": "http://www.example.com/images/logo.png"
            }
        </script>

        <script type="text/javascript">
            jQuery(function() {
                jQuery('.this_is_my_lead_form').on('submit', function() {
                    var d = new Date();
                    var hours = d.getHours();
                    var minutes = d.getMinutes();
                    if (hours <= 9) hours = "0" + hours;
                    if (minutes <= 9) minutes = "0" + minutes;
                });
            });
        </script>

        <script type="text/javascript">
            $(function() {

                var current_date = new Date(); // текущая дата
                current_date.setDate(current_date.getDate())
                curDay = current_date.getDate(11),
                    curMonth = current_date.getMonth(),
                    curYear = current_date.getFullYear();


                $('#counter1').countMe(curYear, curMonth, curDay + 1, '#counter1');
                $('#counter2').countMe(curYear, curMonth, curDay + 1, '#counter2');
            });
        </script>

    </body>

    </html>