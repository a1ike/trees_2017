$(document).ready(function () {

  jQuery(function () {
    var ts = new Date(2016, 3, 19, 23, 0, 0, 0),
      newYear = false;

    jQuery('.countdown').countdown({
      timestamp: ts
    });
  });

  $.fn.countMe = function (year, mounth, day, id) {

    var note = $('#counter'),
      ts = new Date(year, mounth, day),
      newYear = true;

    if ((new Date()) > ts) {
      // Задаем точку отсчета для примера. Пусть будет очередной Новый год или дата через 10 дней.
      // Обратите внимание на *1000 в конце - время должно задаваться в миллисекундах
      ts = (new Date()).getTime() + ts * 60 * 60 * 1000;
      newYear = false;
    }

    $(id).countdown({
      timestamp: ts,
      callback: function (days, hours, minutes, seconds) {

        var message = '';

        message += 'Дней: ' + days + ', ';
        message += 'часов: ' + hours + ', ';
        message += 'минут: ' + minutes + ' и ';
        message += 'секунд: ' + seconds + ' <br />';

        if (newYear) {
          message += 'осталось до Нового года!';
        }
        else {
          message += 'осталось до момента через 10 дней!';
        }

        note.html(message);
      }
    });

  };

  $('.el-gallery').slick({
    arrows: true,
    nextArrow: '<img src="./images/icon_right.png" class="slick-next">',
    prevArrow: '<img src="./images/icon_left.png" class="slick-prev">',
    dots: false,
    autoplay: false,
    speed: 300,
    slidesToShow: 5,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });

  $('.el-goods__card-slick').slick({
    arrows: true,
    nextArrow: '<img src="./images/icon_small_right.png" class="slick-next">',
    prevArrow: '<img src="./images/icon_small_left.png" class="slick-prev">',
    dots: false,
    autoplay: false,
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 1
  });

  $('a[href="#s1"]').click(function () {
    $.scrollTo('#s1', 500, {
      offset: -50,
      duration: 500
    });
  });

  $('a[href="#s2"]').click(function () {
    $.scrollTo('#s2', 500, {
      offset: -200,
      duration: 500
    });
  });

  $('a[href="#s3"]').click(function () {
    $.scrollTo('#s3', 500, {
      offset: -50,
      duration: 500
    });
  });

  $('a[href="#s4"]').click(function () {
    $.scrollTo('#s4', 500, {
      offset: -50,
      duration: 500
    });
  });
  
    $('a[href="#forma"]').click(function () {
      $.scrollTo('#forma', 500, {
        offset: -50,
        duration: 500
      });
    });

  $(window).scroll(function () {
    if ($(this).scrollTop() > 50) {
      $('.el-header').css({ 'padding': '0 0 2px 0' });
    } else {
      $('.el-header').css({ 'padding': '10px 0' });
    }
  });

  $('form').submit(function (e) {
    e.preventDefault();
    var f = $(this);
    $('.ierror', f).removeClass('ierror');
    var avtor = $('input[name="avtor"]', f).val();
    var tel = $('input[name="tel"]', f).val();
    var email = $('input[name="e-mail"]', f).val();
    var error = false;
    if (tel == '') {
      $('input[name="avtor"]', f).addClass('ierror');
      error = true;
    }
    if (tel == '') {
      $('input[name="tel"]', f).addClass('ierror');
      error = true;
    }
    if (email == '') {
      $('input[name="e-mail"]', f).addClass('ierror');
      error = true;
    }
    if (error) {
      return false;
    }
    $.ajax({
      type: 'POST',
      url: 'mail.php',
      data: $(this).serialize(),
    }).done(function (data) {
      if (data.response == 'ok') {
        window.location.href = 'https://docs.google.com/spreadsheets/d/1s28svuFn5OaqjAQ597vgi4mEz8GgdcKhDna_u1Vpt9Q/edit#gid=661409618';
      }
      else {
        alert('Ошибка! Заявка не отправлена, повторите запрос позже');
      }
    });
    return false;
  });

});

$('a[href="#s1"]').click(function () {

  $.scrollTo('#s1', 500, {
    offset: -50,
    duration: 500
  });

  $('html, body').animate({ scrollTop: '0px' }, 300);

});
