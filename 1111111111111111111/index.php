<?php
 
// Поместите эти строчки в начало страницы
define('ROISTAT_AB_AUTO_SUBMIT', false);
require_once __DIR__ . '/ABTest.php'; // Путь до ABTest.php
ABTest::instance()->activateTest('ab_three_versions'); // Название теста из tests.php
 
// Переадресация должна работать в самом верху кода, до загрузки контента страницы
$v = ABTest::instance()->getTestValue('ab_three_versions');
if ($v !== 'main'): // Если вариант new, то запускается переадресация
   $param = !empty($_SERVER['QUERY_STRING']) ? "?" . $_SERVER['QUERY_STRING'] : ""; // Здесь сохраняются все метки, которые были в ссылке
  
   setcookie("roistat_referer", isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : "");
   ABTest::instance()->submit();

   header("Location: http://xn--n1acj.xn----8sbe7aeq0a0ita.xn--p1ai/$v/{$param}"); // Замените site.ru на адрес сайта, куда будет вести переадресация
   exit();
   endif;
ABTest::instance()->submit(); 





$keywords = array(
	"elki-optom",
	"iskysstvennye-elki-optom",
	"novogodnie-elki-optom",
	"kupit-elki-optom",
	"kupit-iskysstvennye-elki-optom",
	"iskysstvennye-elki-optom-ot-proizvoditelya",
	"iskysstvennye-elki-optom-ot-postavshika",
	"iskysstvennye-elki-optom-v-moskve",
	"postavshchiki-iskysstvennyh-elok",
	"iskysstvennye-elki-optom-so-sklada",
	"kupit-iskysstvennye-elki-optom-v-moskve",
	"iskysstvennye-elki-optom-s-garantiej"
);
$i=0;
?>

    <!DOCTYPE html>
    <html lang="ru">

    <head>
        <meta charset="utf-8">
        <meta name="description" content="Искусственные ёлки оптом от производителя, в наличии в Москве! Бесплатная франшиза! Купить искусственные ёлки оптом от 120 руб. ">
        <meta name="keywords" content="Купить искусственные елки оптом, куплю искусственные елки оптом, продажа искусственные елки оптом, продажа новогодних елки оптом, искусственные елки цена оптом, искусственные елки оптом Москва, искусственные елки оптом по РФ, искусственные елки оптом производитель, куплю искусственные елки оптом Москва, где купить искусственные елки оптом, покупка искусственные елки оптом, закупка искусственные елки оптом, оптовый склад искусственных елок, оптовая продажа искусственных елок, искусственные елки оптом от производителя, ёлки оптом, искусственные ёлки, искусственные ёлки купить оптом, искусственные ели оптом, искусственные сосны оптом, ели оптом, искусственные ели">
        <meta name="title" content="Оптовая продажа искусственных ёлок">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="yandex-verification" content="baf9d1ebf8e34163" />
        <meta name="google-site-verification" content="COO5NAV4wGsg4nJgMpcblIdJEeufdXH_N4WbiZiLUU8" />

        <title>Искусственные ёлки оптом от производителя</title>

        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
        <link rel="icon" href="/favicon.ico" type="image/x-icon">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" integrity="sha256-jySGIHdxeqZZvJ9SHgPNjbsBP8roij7/WjgkoGTJICk=" crossorigin="anonymous" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css" integrity="sha256-WmhCJ8Hu9ZnPRdh14PkGpz4PskespJwN5wwaFOfvgY8=" crossorigin="anonymous" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.theme.default.min.css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.carousel.min.css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.css" integrity="sha256-UpdOHyyfsvF5Uu6BhbsYQHd1aCNIvxhICDFjz4QbENo=" crossorigin="anonymous" />

<!-- <script "type=text/javascript">var random = Math.random();if (random >= '0.5') {var rand = 1;}else {var rand = 2;};var ab_cookie = getCookie('ab_cookie');switch (ab_cookie) {case 'a':break;case 'b':redirectToB();break;default:if(rand == 1) {setCookie('ab_cookie', 'a', 3);}else {setCookie('ab_cookie', 'b', 3);redirectToB();}}function redirectToB() {window.location = "http://xn--n1acj.xn----8sbe7aeq0a0ita.xn--p1ai/2/";}function setCookie(c_name, value, exdays) {var exdate = new Date();exdate.setDate(exdate.getDate() + exdays);var c_value = encodeURI(value) +((exdays == null) ? "" : ("; expires=" + exdate.toUTCString()));document.cookie = c_name + "=" + c_value;}function getCookie(c_name) {var i, x, y, ARRcookies = document.cookie.split(";");for (i = 0; i < ARRcookies.length; i++) {x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);x = x.replace(/^s+|s+$/g, "");if (x == c_name) {return decodeURI(y);}}}</script> -->


        <link rel="stylesheet" href="styles/main.css">
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-N4Z7RCD');</script>
<!-- End Google Tag Manager -->

<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = 'https://vk.com/rtrg?p=VK-RTRG-40670-5uB4B';</script>

<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '292940217885777');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=292940217885777&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
    </head>
    <body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N4Z7RCD"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
        <!--[if IE]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

        <header class="el-header">
            <div class="container">
                <div class="el-header__logo">
                    <div class="el-header__logo-title">Твоя-ёлка.рф</div>
                    <h1>
                        Искусственные елки оптом
                    </h1>
                </div>
                <nav class="el-header__nav">
                 <a href="#s5">Франшиза</a>
                    <a href="#s1">Ёлки</a>
                 <a href="#s3">Отзывы </a>

                    <a href="#s2">Фото и видео</a>
                   
                    <a href="#forma" onClick="yaCounter46326543.reachGoal('order1');"><strong>Скачать прайс</strong></a>
                </nav>
                <div class="el-header__row">
                    <div class="el-header__row-line">
                        <div>
                            <img src="./images/icon_phone.png" alt="Телефон">
                        </div>
                        <div>
                            <a href="tel:+74996382991"><strong>8 (499) 638-29-91</strong></a><br>
                        </div>
                    </div>
                    <div class="el-header__row-line">
                        <div>
                            <img src="./images/icon_phone.png" alt="Телефон">
                        </div>
                        <div>
                            <a href="tel:+79858503872">8 (985) 850-38-72</a>
                            <a href="#" class="el-header__small-social">
                                <img src="./images/icon_wu.png" alt="WhatsUp">
                            </a>
                            <a href="#" class="el-header__small-social">
                                <img src="./images/icon_tg.png" alt="Telegram">
                            </a>
                            <a href="#" class="el-header__small-social">
                                <img src="./images/icon_vb.png" alt="Viber">
                            </a>

                        </div>

                    </div>
                    <div class="el-header__row-line">
                        <p>
                            Шоу-рум: Смоленский бульвар 17с5<br> Склад/самовывоз: Волгоградский проспект 42к5
                        </p>
                    </div>
                </div>
            </div>
        </header>

        <div class="container">
            <div class="el-hello">
                <div class="activity res" id="forma">
                    <h1 class="din_bold">Искусственные ёлки оптом от 95 руб<br>ликвидация склада!</h1><br>
                    <span class="diin_bold"><span class="yellow">  в наличии 10000 ёлок  |  бесплатная франшиза  |  доставка за 2 дня </span></span>
                </div>
            </div>
        </div>

        <div class="form_wrap" >
            <div class="count_down">
                <div class="first_line clear_self">
                    <div class="num big_noodle">- 51%</div>
                    <div class="din_bold">ЛИКВИДАЦИЯ СКЛАДА</div> СПЕЦПРЕДЛОЖЕНИЕ ОГРАНИЧЕНО
                </div>
                <div class="counter clear_self">
                    <div class="desc">ДО КОНЦА<br>АКЦИИ:</div>
                    <div data-year="2016" data-month="12" data-day="22" class="this-box5-timer" id="counter1"></div>
                </div>
            </div>
            <div class="form_bg bbox">
                <div class="name din_medium">
                    СКАЧАЙТЕ ПРАЙС 2017 ГОДА<br> и получите скидку до 51%
                </div>
                <form action="mail.php" method="post" >
                    <input type="text" name="avtor" placeholder="Имя" required>
                    <input id="phone" type="text" name="tel" placeholder="Телефон" required>
                    <input id="email" type="text" name="email" placeholder="e-mail" required>
                    <input type="submit" value="СКАЧАТЬ ПРАЙС" class="but bbox din_bold" onSubmit="fbq('track', 'Lead'); yaCounter46326543.reachGoal('forma1');">
                </form>
                <div class="note din">заполняя формы вы соглашаетесь с политикой конфиденциальности</div>
            </div>
        </div>

        <!--<div class="container">
        <div class="el-companies">

            <img src="./images/company_1.png" alt="Название компании">
            <img src="./images/company_2.png" alt="Название компании">
            <img src="./images/company_3.png" alt="Название компании">
            <img src="./images/company_1.png" alt="Название компании">
            <img src="./images/company_2.png" alt="Название компании">
            <img src="./images/company_3.png" alt="Название компании">

        </div>
    </div> -->

        <div class="container">
            <div class="el-why">

                <div class="el-why__card el-why__card_1">
                    <img src="./images/why_1.png" alt="5 лет на рынке">
                </div>

                <div class="el-why__card el-why__card_2">
                    <img src="./images/why_2.png" alt="Поставки по России и СНГ">
                </div>

                <div class="el-why__card el-why__card_3">
                    <img src="./images/why_3.png" alt="Обеспечиваем ёлками лидеров розничного рынка">
                </div>

            </div>
        </div>

        <div id="s5" class="el-price">
            <div class="container">
                <h2 class="el-price__title"><span>Продажа искусственных елок</span> - бесплатная франшиза</h2>
                <p class="el-price__sub-title">
                <strong>Франшиза</strong> от нашей компании – это возможность открыть собственное дело по реализации искусственных елок и получить прибыль уже до Нового года! Пятилетний опыт продаж позволил нам разработать эффективные пошаговые инструкции и проверенные на практике инструменты, которыми мы поделимся с вами совершенно бесплатно.
                </p>
                <div class="el-price__cards">

                    <section class="el-price__card">
                        <header class="el-price__card-header">
                            <p>при закупке</p>
                            <div class="el-price__card-price">от 155 000 руб</div>
                            <p>получаете:</p>
                        </header>
                        <div>
                            <div class="el-price__card-feature">
                                Все заявки по вашему региону

                            </div>
                            <div class="el-price__card-feature">
                                Доп. оптовая скидка до 60%

                            </div>
                            <div class="el-price__card-feature">
                                Сайт под ключ
                            </div>
                            <div class="el-price__card-feature">
                                Рекламные компании Яндекс Директ + Google Adwords
                            </div>

                            <div class="el-price__card-feature">
                                Руководство по продажам на Avito
                            </div>
                            <div class="el-price__card-feature">
                                Рекламные компании: Instagram, Facebook- таргетинг
                            </div>
                            <div class="el-price__card-feature">
                                Руководство по продажам в соц сетях
                            </div>
                            <div class="el-price__card-feature">
                                Скрипты продаж
                            </div>
                            <div class="el-price__card-feature">
                                Автоматизация отдела продаж
                            </div>
                            <div class="el-price__card-feature">
                                Поддержка 24/7
                                <div class="el-price__card-info">
                                    (личный менеджер, обучающие вебинары)
                                </div>
                            </div>
                            <div class="el-price__card-feature">
                                Промо-материалы
                                <ul>
                                    <li>Дизайн гр. VK и Instagram</li>
                                    <li>Макеты листовок (флаера и расклейка)</li>
                                    <li>Макеты продающих ценников</li>
                                    <li>Шаблон объявления для размещения на досках</li>
                                    <li>Набор фото и видео</li>
                                    <li>Пример рекламы на радио</li>
                                </ul>
                            </div>
                        </div>
                    </section>

                    <section class="el-price__card">
                        <header class="el-price__card-header">
                            <p>при закупке</p>
                            <div class="el-price__card-price">от 55 000 руб</div>
                            <p>получаете:</p>
                        </header>
                        <div>
                            <div class="el-price__card-feature">
                                Сайт под ключ
                            </div>
                            <div class="el-price__card-feature">
                                Рекламные компании Яндекс Директ + Google Adwords
                            </div>
                            <div class="el-price__card-feature">
                                Скрипты продаж
                            </div>
                            <div class="el-price__card-feature">
                                Рекламные компании: Instagram, Facebook- таргетинг
                            </div>
                            <div class="el-price__card-feature">
                                Личный менеджер
                            </div>
                            <div class="el-price__card-feature">
                                24 проверенных источника привлечения клиентов
                            </div>
                            <div class="el-price__card-feature">
                                Промо-материалы
                                <ul>
                                    <li>Макеты листовок (флаера и расклейка)</li>
                                    <li>Макеты продающих ценников</li>
                                    <li>Шаблон объявления для размещения на досках</li>
                                    <li>Набор фото и видео</li>
                                </ul>
                            </div>
                        </div>
                    </section>

                    <section class="el-price__card">
                        <header class="el-price__card-header">
                            <p>при закупке</p>
                            <div class="el-price__card-price">от 15 000 руб</div>
                            <p>получаете:</p>
                        </header>
                        <div>
                        <div class="el-price__card-feature">
                                Сайт под ключ
                            </div>
                            <div class="el-price__card-feature">
                                24 проверенных источника привлечения клиентов 
                            </div>
<div class="el-price__card-feature">
                                Рекламные компании Яндекс Директ
                            </div>
                            <div class="el-price__card-feature">
                                Промо-материалы
                                <ul>
                                    <li>Макеты листовок (флаера/расклейка)</li>
                                    <li>Шаблон объявления для размещения на досках</li>
                                    <li>Набор фото и видео</li>
                                </ul>
                            </div>
                        </div>
                    </section>

                </div>
                <a href="#forma" class="el-price__download" onClick="yaCounter46326543.reachGoal('order2');">
                    <img src="./images/icon_xls.png" alt="Скачать">
                    <span>скачать оптовый прайс</span>
                </a>
            </div>
        </div>

        <div class="el-model">
            <div class="container">
                <h2 class="el-model__title">Новогодние елки оптом - <span>готовая бизнес-модель</span></h2>
                <p class="el-price__sub-title">
                Планируете организовать успешный <strong>бизнес по продаже искусственных елок</strong>? Наша компания бесплатно предоставляет полностью готовый к внедрению бизнес-план, который позволит открыть и запустить собственное прибыльное предприятие всего за 2 дня.
                </p>
                <div class="el-model__cards">

                    <div class="el-model__card">
                        <img src="./images/model_1.png" alt="План">
                        <div class="el-model__card-title">Руководство по бизнесу</div>
                    </div>

                    <div class="el-model__card">
                        <img src="./images/model_2.png" alt="План">
                        <div class="el-model__card-title">Привлечение клиентов</div>
                    </div>

                    <div class="el-model__card">
                        <img src="./images/model_3.png" alt="План">
                        <div class="el-model__card-title">Работа с персоналом</div>
                    </div>

                    <div class="el-model__card">
                        <img src="./images/model_4.png" alt="План">
                        <div class="el-model__card-title">Организация доставки</div>
                    </div>

                    <div class="el-model__card">
                        <img src="./images/model_5.png" alt="План">
                        <div class="el-model__card-title">Работа с отдела продаж</div>
                    </div>

                    <div class="el-model__card">
                        <img src="./images/model_6.png" alt="План">
                        <div class="el-model__card-title">Учет финансов</div>
                    </div>

                    <div class="el-model__card">
                        <img src="./images/model_7.png" alt="План">
                        <div class="el-model__card-title">Маркетинговые материалы</div>
                    </div>

                    <div class="el-model__card">
                        <img src="./images/model_8.png" alt="План">
                        <div class="el-model__card-title">Шаблоны, макеты, дизайн</div>

                    </div>

                </div>
            </div>
        </div>

        <div id="s1" class="el-goods">
            <div class="container">
                <h2 class="el-goods__title"><span>Искусственные ели оптом<br></span> в Москве, Санкт-Петербурге и по всей России</h2>
                <p class="el-goods__sub-title">
                Хотите купить <strong>искусственные елки оптом</strong> по выгодной стоимости в Москве и других городах России? Как <strong>прямой поставщик</strong> новогодних красавиц, мы <strong>дешево</strong> реализуем самые роскошные и продаваемые позиции елей и предлагаем сопутствующую продукцию для до-продажи.
                </p>
                <div id="el-tab" class="el-goods__tabs">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#1" data-toggle="tab">Ёлки</a></li>
                        <!--<li><a href="#2" data-toggle="tab">Гирлянды</a></li>-->
                        <!--<li><a href="#3" data-toggle="tab">Мишура</a></li>-->
                        <!--<li><a href="#4" data-toggle="tab">Игрушки</a></li>-->
                    </ul>
                    <div class="tab-content">

                        <div class="tab-pane active" id="1">
                            <div class="el-goods__cards">

                                <div class="el-goods__card">
                                    <div class="el-goods__card-left">
                                        <div class="el-goods__card-tree">
                                            <a data-fancybox="tree" href="./images/trees/60.png"><img src="./images/trees/60.png" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            <div class="el-goods__card-zoom"></div>
                                        </div>
                                        <div class="el-goods__card-slick">
                                            <div>
                                                <a data-fancybox="tree" href="./images/trees/60.png"><img src="./images/trees/60.png" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                           <!-- <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div> -->
                                            
                                        </div>
                                    </div>
                                    <div class="el-goods__card-right">
                                        <div class="el-goods__card-title">
                                            Ёлочка 60 см
                                        </div>
                                        <div class="el-goods__card-description">
                                            <p>
                                                Высота: 60 см<br> Вес: 0,4 кг<br> Размер упаковки: полиэтилен
                                            </p>
                                            <div class="el-goods__card-price">
                                                Цена: от 95 руб.
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="el-goods__card">
                                    <div class="el-goods__card-left">
                                        <div class="el-goods__card-tree">
                                            <a data-fancybox="tree" href="./images/trees/60.png"><img src="./images/trees/60.png" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            <div class="el-goods__card-zoom"></div>
                                        </div>
                                        <div class="el-goods__card-slick">
                                            <div>
                                                <a data-fancybox="tree" href="./images/trees/60.png"><img src="./images/trees/60.png" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                           <!-- <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div> -->
                                           
                                        </div>
                                    </div>
                                    <div class="el-goods__card-right">
                                        <div class="el-goods__card-title">
                                            Ёлочка 90 см
                                        </div>
                                        <div class="el-goods__card-description">
                                            <p>
                                                Высота: 90 см<br> Вес: 0,7 кг<br> Размер упаковки: полиэтилен
                                            </p>
                                            <div class="el-goods__card-price">
                                                Цена: от 180 руб.
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="el-goods__card">
                                    <div class="el-goods__card-left">
                                        <div class="el-goods__card-tree">
                                            <a data-fancybox="tree" href="./images/trees/120.png"><img src="./images/trees/120.png" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            <div class="el-goods__card-zoom"></div>
                                        </div>
                                        <div class="el-goods__card-slick">
                                            <div>
                                                <a data-fancybox="tree" href="./images/trees/120.png"><img src="./images/trees/120.png" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                           <!-- <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div> -->
                                            
                                        </div>
                                    </div>
                                    <div class="el-goods__card-right">
                                        <div class="el-goods__card-title">
                                            Ёлочка 120 см
                                        </div>
                                        <div class="el-goods__card-description">
                                            <p>
                                                Высота: 120 см<br> Вес: 2,5 кг<br> Размер упаковки: 65см х 14см х 9см
                                            </p>
                                            <div class="el-goods__card-price">
                                                Цена: от 440 руб.
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="el-goods__card">
                                    <div class="el-goods__card-left">
                                        <div class="el-goods__card-tree">
                                            <a data-fancybox="tree" href="./images/trees/150.png"><img src="./images/trees/150.png" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            <div class="el-goods__card-zoom"></div>
                                        </div>
                                        <div class="el-goods__card-slick">
                                            <div>
                                                <a data-fancybox="tree" href="./images/trees/150.png"><img src="./images/trees/150.png" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <!-- <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div> -->
                                          
                                        </div>
                                    </div>
                                    <div class="el-goods__card-right">
                                        <div class="el-goods__card-title">
                                            Ёлочка 150 см
                                        </div>
                                        <div class="el-goods__card-description">
                                            <p>
                                                Высота: 150 см<br> Вес: 3,5 кг<br> Размер упаковки: 66см х 14см х 15см
                                            </p>
                                            <div class="el-goods__card-price">
                                                Цена: от 805 руб.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="el-goods__card">
                                    <div class="el-goods__card-left">
                                        <div class="el-goods__card-tree">
                                            <a data-fancybox="tree" href="./images/trees/180.png"><img src="./images/trees/180.png" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            <div class="el-goods__card-zoom"></div>
                                        </div>
                                        <div class="el-goods__card-slick">
                                            <div>
                                                <a data-fancybox="tree" href="./images/trees/180.png"><img src="./images/trees/180.png" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                           <!-- <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div> -->
                                            
                                        </div>
                                    </div>
                                    <div class="el-goods__card-right">
                                        <div class="el-goods__card-title">
                                            Ёлочка 180 см
                                        </div>
                                        <div class="el-goods__card-description">
                                            <p>
                                                Высота: 180 см<br> Вес: 4,5 кг<br> Размер упаковки: 68см х 16см х 18см
                                            </p>
                                            <div class="el-goods__card-price">
                                                Цена: от 1100 руб.
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="el-goods__card">
                                    <div class="el-goods__card-left">
                                        <div class="el-goods__card-tree">
                                            <a data-fancybox="tree" href="./images/trees/210.png"><img src="./images/trees/210.png" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            <div class="el-goods__card-zoom"></div>
                                        </div>
                                        <div class="el-goods__card-slick">
                                            <div>
                                                <a data-fancybox="tree" href="./images/trees/210.png"><img src="./images/trees/210.png" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                           <!-- <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div> -->
                                            
                                        </div>
                                    </div>
                                    <div class="el-goods__card-right">
                                        <div class="el-goods__card-title">
                                            Ёлочка 210 см
                                        </div>
                                        <div class="el-goods__card-description">
                                            <p>
                                                Высота: 210 см<br> Вес: 6 кг<br> Размер упаковки: 70см х 19см х 21см
                                            </p>
                                            <div class="el-goods__card-price">
                                                Цена: от 1480 руб.
                                            </div>
                                        </div>
                                    </div>
                                </div>



                                <div class="el-goods__card">
                                    <div class="el-goods__card-left">
                                        <div class="el-goods__card-tree">
                                            <a data-fancybox="tree" href="./images/trees/240.png"><img src="./images/trees/240.png" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            <div class="el-goods__card-zoom"></div>
                                        </div>
                                        <div class="el-goods__card-slick">
                                            <div>
                                                <a data-fancybox="tree" href="./images/trees/240.png"><img src="./images/trees/240.png" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                           <!-- <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div> -->
                                          
                                        </div>
                                    </div>
                                    <div class="el-goods__card-right">
                                        <div class="el-goods__card-title">
                                            Ёлочка 240 см
                                        </div>
                                        <div class="el-goods__card-description">
                                            <p>
                                                Высота: 240 см<br> Вес: 7,5 кг<br> Размер упаковки: 75см х 28см х 25см
                                            </p>
                                            <div class="el-goods__card-price">
                                                Цена: от 2070 руб.
                                            </div>
                                        </div>
                                    </div>
                                </div>









                            </div>
                        </div>

                        <div class="tab-pane" id="2">
                            <div class="el-goods__cards">

                                <div class="el-goods__card">
                                    <div class="el-goods__card-left">
                                        <div class="el-goods__card-tree">
                                            <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            <div class="el-goods__card-zoom"></div>
                                        </div>
                                        <div class="el-goods__card-slick">
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="el-goods__card-right">
                                        <div class="el-goods__card-title">
                                            Ёлочка 180 см
                                        </div>
                                        <div class="el-goods__card-description">
                                            <p>
                                                Высота ёлки: 180 см<br> Тип елки: Искусственные елки<br> Длина хвои ветки: 0.45 см<br> Защита от огня: нет<br> Количество веток: шт
                                            </p>
                                            <div class="el-goods__card-price">
                                                Цена: от 111 руб.
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="el-goods__card">
                                    <div class="el-goods__card-left">
                                        <div class="el-goods__card-tree">
                                            <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            <div class="el-goods__card-zoom"></div>
                                        </div>
                                        <div class="el-goods__card-slick">
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="el-goods__card-right">
                                        <div class="el-goods__card-title">
                                            Ёлочка 210 см
                                        </div>
                                        <div class="el-goods__card-description">
                                            <p>
                                                Высота ёлки: 210 см<br> Тип елки: Искусственные елки<br> Длина хвои ветки: 0.45 см<br> Защита от огня: нет<br> Количество веток: шт
                                            </p>
                                            <div class="el-goods__card-price">
                                                Цена: от 111 руб.
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="tab-pane" id="3">
                            <div class="el-goods__cards">

                                <div class="el-goods__card">
                                    <div class="el-goods__card-left">
                                        <div class="el-goods__card-tree">
                                            <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            <div class="el-goods__card-zoom"></div>
                                        </div>
                                        <div class="el-goods__card-slick">
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="el-goods__card-right">
                                        <div class="el-goods__card-title">
                                            Ёлочка 240 см
                                        </div>
                                        <div class="el-goods__card-description">
                                            <p>
                                                Высота ёлки: 50 см<br> Тип елки: Искусственные елки<br> Длина хвои ветки: 0.45 см<br> Защита от огня: нет<br> Количество веток: шт
                                            </p>
                                            <div class="el-goods__card-price">
                                                Цена: от 111 руб.
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="el-goods__card">
                                    <div class="el-goods__card-left">
                                        <div class="el-goods__card-tree">
                                            <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            <div class="el-goods__card-zoom"></div>
                                        </div>
                                        <div class="el-goods__card-slick">
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                            <div>
                                                <a data-fancybox="tree" href="./images/tree_1.jpg"><img src="./images/tree_1.jpg" alt="<? if($i>count($keywords)-1) { $i=0; } print $keywords[$i]; $i++; ?>"></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="el-goods__card-right">
                                        <div class="el-goods__card-title">
                                            Ёлочка 50 см
                                        </div>
                                        <div class="el-goods__card-description">
                                            <p>
                                                Высота ёлки: 50 см<br> Тип елки: Искусственные елки<br> Длина хвои ветки: 0.45 см<br> Защита от огня: нет<br> Количество веток: шт
                                            </p>
                                            <div class="el-goods__card-price">
                                                Цена: от 111 руб.
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
</div>
                <a href="#forma" class="el-price__download" onClick="yaCounter46326543.reachGoal('order3');">
                    <img src="./images/icon_xls.png" alt="Скачать">
                    <span>скачать полный прайс</span>
                </a>
            </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="el-nice">

            <div class="elka" style="background-color: rgb(243, 243, 243); width: 100%; margin-bottom: -80px; padding-bottom: 80px; padding-top: 24px; position: relative; text-align: center; display: inline-block;">
                <h5 class="el-title_h5">ИСКУССТВЕННЫЕ ЕЛКИ ОТ ПРОИЗВОДИТЕЛЯ<br><span class="din_bold">С ГАРАНТИЕЙ КАЧЕСТВА</span></h5>
                <div style="position: relative; display: inline-block; text-align: center; width: 980px; float: none;" class="content">
                    <img alt="искусственные елки оптом" src="./images/why1.png">
                    <div style="left: -95px; top: 50px;" class="zagotext">
                        <h3>ПЫШНЫЕ ВЕТКИ</h3>
                        <p>За счет роскошной пышности ёлочка выглядит очень эффектно. Длина иголок 5-7 см.</p>
                    </div>
                    <div style="top: 50px; left: 870px;" class="zagotext">
                        <h3>ЖИВЫЕ ШИШКИ</h3>
                        <p>В комплекте с ёлочкой прилагаются живые шишки для более эффектного вида будущей ёлочки</p>
                    </div>
                    <div style="left: -80px; top: 535px;" class="zagotext">
                        <h3>ЗАСНЕЖЕННЫЕ ВЕТКИ</h3>
                        <p>Пышные ветки с снежным напылением делают ёлочку неотразимой.</p>
                    </div>
                    <div style="left: 850px; top: 519px;" class="zagotext">
                        <h3>ПРОЧНЫЕ КРЕПЛЕНИЯ</h3>
                        <p>Елка имеет отличную устойчивость за счет множества прочных креплений. Ее легко собирать\разбирать и хранить</p>
                    </div>
                    <div style="left: -50px; top: 778px;" class="zagotext">
                        <h3>УСТОЙЧИВАЯ ПОДСТАВКА</h3>
                        <p>Ёлочки имеют устойчивое металлическое основание. Она не упадет под весом игрушек</p>
                    </div>
                    <div style="left: 611px; top: 780px;" class="zagotext">
                        <h3>ДОЛГОВЕЧНА И ЭКОЛОГИЧНА</h3>
                        <p>Ёлочка производится из качественных и экологически чистых материалов, которые прошли контроль качества в РФ.</p>
                    </div>
                </div>
            </div>
        </div>

        <div id="s3" class="el-reviews">
            <div class="container">
                <div class="el-reviews__title"><span>Отзывы</span> наших партнёров</div>
               <!-- <p class="el-reviews__sub-title">
                    Мастера - профессионалы в своей области - регулярно проходят курсы повышения квалификации и аттестацию. Все они - постоянные сотрудники.
                </p> -->
                <div class="el-reviews__cards">

                    <div class="el-reviews__card">
                        <div class="el-reviews__card-left">
                            <img src="./images/mikhail.png" class="el-reviews__card-photo">
                            <!--<a data-fancybox="reviews" href="https://www.youtube.com/embed/mEErTZTHBuY">
                                <img src="./images/youtube.png" alt="Видео">
                                <p class="el-reviews__card-show">видео отзыв <img src="./images/icon_show.png" alt="Смотреть видео"></p>
                            </a>-->
                        </div>
                        <div class="el-reviews__card-right">
                            <div class="el-reviews__card-title">
                                <strong>Интернет магазин и розничная точка</strong> Прибыль за 1 мес: 196 000руб
                            </div>
                            <p>
                                Благодаря вашей компании с нуля открыл новое направление в бизнесе. За 3-4 дня открыл точку шоу-рум и оформил все как вы советовали. Популярные размеры ёлок закончились уже на следующий день в обед, т.к. на первый раз мало закупил)))

С интернета тоже идут заказы в основном с Яндекса и Авито. На следующий год буду покупать эксклюзив на свой город!;)


                            </p>
                            <p>
Вторую партию закупил - получил отличную скидку и неожиданные бонусы!! А еще у вас профессиональные менеджеры - это большой плюс:))                            </p>
                            <div class="el-reviews__card-from">
                                <strong>Михаил,</strong> г. Нижний Новгород
                            </div>
                        </div>
                    </div>

                    <div class="el-reviews__card">
                        <div class="el-reviews__card-left">
                            <img src="./images/alexey.png" class="el-reviews__card-photo">
                           <!-- <a data-fancybox="reviews" href="./images/paper.png">
                                <img src="./images/paper.png" alt="Сертификат">
                                <p class="el-reviews__card-show">Благодарность <img src="./images/icon_show.png" alt="Смотреть видео"></p>
                            </a> -->
                        </div>
                        <div class="el-reviews__card-right">
                            <div class="el-reviews__card-title">
                                <strong>Сеть розничных магазинов</strong> Выручка за 1.5 мес: 1 791 000руб
                            </div>
                            <p>
                                Замеры объёма продаж всех представленных искусственных сосен, проводимые аудиторской комиссией нашей сети магазинов, показали, что искусственные сосны поставщика "Твоя-ёлка" продаются в 1,7 раза лучше других поставщиков уже на протяжении последних 2-х
                                лет!
                            </p>
                            <p>
                                От себя хочу добавить: весь процессы закупок и поставок проходит гладко и без нервов, за что очень Вам признателен!
                            </p>
                            <div class="el-reviews__card-from">
                                <strong>Алексей,</strong> г. Санкт-Петербург
                            </div>
                        </div>
                    </div>

                    <div class="el-reviews__card">
                        <div class="el-reviews__card-left">
                            <img src="./images/denis.png" class="el-reviews__card-photo">
                            
                        </div>
                        <div class="el-reviews__card-right">
                            <div class="el-reviews__card-title">
                            

                                <strong>Интернет-магазин</strong> Прибыль за декабрь: 206 000 руб
                            </div>
                            <p>
                                Сотрудничаю с ребятами уже 3й год! 
Задержек с поставками не было. Ёлки всегда в наличии и отличного качества, брак минимален и с его заменой нет проблем. Ребята дают по сути готовый бизнес: после использования наработок увеличил продажи почти в 2 раза.
</p>
                            <p>
Всегда ответят, помогут, подскажут. Видно что работают на качество и долгосрочное сотрудничество.                            </p>
                            <div class="el-reviews__card-from">
                                <strong>Денис,</strong> г. Москва
                            </div>
                        </div>
                    </div>

                    <div class="el-reviews__card">
                        <div class="el-reviews__card-left">
                            <img src="./images/marat.png" class="el-reviews__card-photo">
                         <!--   <a data-fancybox="reviews" href="https://www.youtube.com/embed/mEErTZTHBuY">
                                <img src="./images/youtube.png" alt="Видео">
                                <p class="el-reviews__card-show">видео отзыв<img src="./images/icon_show.png" alt="Смотреть видео"></p>
                            </a>-->
                        </div>
                        <div class="el-reviews__card-right">
                            <div class="el-reviews__card-title">
                                <strong>Оптовая и розничная продажа ёлок</strong> Прибыль за 2мес: 487 000руб
                            </div>
                            <p>
                                Здравствуйте! Спасибо за плодотворное сотрудничество! Благодаря коллегам из Твоя-ёлка быстро организовал оптовую поставку ёлок у себя в регионе. Спасибо ребята, что помогли на старте с доставкой и хорошими условиями!

                            </p>
                            <p>
Одним словом, я доволен сотрудничеством
                            </p>
                            <div class="el-reviews__card-from">
                                <strong>Марат,</strong> г. Волгоград
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="container">
            <div class="el-friends">
                <div class="clients res">
                    <p><span class="el-reviews__card-title">С КЕМ МЫ  СОТРУДНИЧАЕМ</span></p>
                    <li>
                        <a href="#a"><img src="images/partner1.png" alt=""></a>
                    </li>
                    <li>
                        <a href="#a"><img src="images/partner3.png" alt=""></a>
                    </li>
                    <li>
                        <a href="#a"><img src="images/partner5.png" alt=""></a>
                    </li>
                    <li>
                        <a href="#a"><img src="images/partner2.png" alt=""></a>
                    </li>
                </div>
            </div>
        </div>

        <div class="form_wrap" >
            <div class="count_down">
                <div class="first_line clear_self">
                    <div class="num big_noodle">- 51%</div>
                    <div class="din_bold">ЛИКВИДАЦИЯ СКЛАДА!</div> СПЕЦПРЕДЛОЖЕНИЕ ОГРАНИЧЕНО
                </div>
                <div class="counter clear_self">
                    <div class="desc">ДО КОНЦА<br>АКЦИИ:</div>
                    <div data-year="2016" data-month="12" data-day="22" class="this-box5-timer" id="counter1"></div>
                </div>
            </div>
            <div class="form_bg bbox">
                <div class="name din_medium">
                    СКАЧАЙТЕ ПРАЙС 2017 ГОДА<br> и получите скидку до 51%
                </div>
                <form action="mail.php" method="post" class="this_is_my_lead_form">
                    <input type="text" name="avtor" placeholder="Имя" class="bbox picted name name_lead_telegram" required>
                    <input id="phone2" type="text" name="tel" placeholder="Телефон" class="bbox picted tel phone_lead_telegram" required>
                    <input id="email2" type="text" name="email" placeholder="e-mail" class="bbox picted mail e-mail_lead_telegram" required>
                    <input type="submit" value="СКАЧАТЬ ПРАЙС" class="but bbox din_bold" onSubmit="fbq('track', 'Lead'); yaCounter46326543.reachGoal('forma1');">
                </form>
                <div class="note din">заполняя формы вы соглашаетесь с политикой конфиденциальности</div>
            </div>
        </div>

        <div id="s4" class="el-profit">
            <div class="container">
                <div class="el-profit__title"><span>ТВОЯ-ЁЛКА.РФ НАДЕЖНЫЙ</span><br> ПОСТАВЩИК ИСКУССТВЕННЫХ ЕЛОК ОПТОМ</div>
                <!--<p class="el-profit__sub-title">
                    Мастера - профессионалы в своей области - регулярно проходят курсы повышения квалификации и аттестацию. Все они - постоянные сотрудники.
                </p>-->
                <div class="el-profit__cards">

                    <div class="el-profit__card">
                        <img src="./images/friends_1.png" alt="Лучшая цена">
                        <div class="el-profit__card-title">
                            ЛУЧШАЯ ЗАКУПОЧНАЯ ЦЕНА
                        </div>
                        <p>
                            мы поставляем елки напрямую от производителя, покупая елки у нас вы не переплачиваете спекулянтам!
                        </p>
                    </div>

                    <div class="el-profit__card">
                        <img src="./images/friends_2.png" alt="Лучшаяцена">
                        <div class="el-profit__card-title">
                            ПОМОГАЕМ С РЕАЛИЗАЦИЕЙ
                        </div>
                        <p>
                            бесплатно предоставляем маркетинговый пакет инструментов - от сайта до клиентов по вашему региону!
                        </p>
                    </div>

                    <div class="el-profit__card">
                        <img src="./images/friends_3.png" alt="Лучшаяцена">
                        <div class="el-profit__card-title">
                            ВЫСОКОЕ КАЧЕСТВО ПРОДУКЦИИ
                        </div>
                        <p>
                            мы поставляем только красивые, пушистые, устойчивые елки с индивидуальной упаковкой каждой ветки!
                        </p>
                    </div>

                    <div class="el-profit__card">
                        <img src="./images/friends_4.png" alt="Лучшаяцена">
                        <div class="el-profit__card-title">
                            БЕСПЕРЕБОЙНЫЕ ПОСТАВКИ
                        </div>
                        <p>
                            постоянные, ежедневные поставки в больших объемах, всегда резервируем товар под каждого клиента!
                        </p>
                    </div>

                    <div class="el-profit__card">
                        <img src="./images/friends_5.png" alt="Лучшаяцена">
                        <div class="el-profit__card-title">
                            ГАРАНТИЯ КАЧЕСТВА
                        </div>
                        <p>
                            каждая партия товара проходит проверку на брак. Процент общего брака менее 1%!
                        </p>
                    </div>

                    <div class="el-profit__card">
                        <img src="./images/friends_6.png" alt="Лучшаяцена">
                        <div class="el-profit__card-title">
                            ПУНКТУАЛЬНАЯ ДОСТАВКА
                        </div>
                        <p>
                            мы экономим ваши деньги и время, используя проверенные, дешевые и быстрые транспортные компании!
                        </p>
                    </div>

                </div>
            </div>
        </div>

        <div  class="el-team">
            <div class="container">
                <div   class="el-team__title">наша <span>команда</span></div>
                <!--<p class="el-team__sub-title">
                    Мастера - профессионалы в своей области - регулярно проходят курсы повышения квалификации и аттестацию. Все они - постоянные сотрудники.
                </p>-->
                <div  class="row clearfix">
                    <div  class="col-md-8">
                        <img src="./images/team.jpg" alt="Наша команда">
                    </div>
                    <div  class="col-md-4">
                        <ul>
                            <li>
                                5 лет на рынке поставок ёлок оптом
                            </li>
                            <li>
                                Поставляем ёлки лидерам розничного рынка
                            </li>
                            <li>
                                За 2016 года продали свыше 10000 ед. ёлок
                            </li>
                            <li>
                                Собственные склады, шоу-рум в центре Москвы, в штате 19 человек
                            </li>
                            <li>
                                19 активных представительств по России и СНГ 
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div id="s2" style="position: relative;" class="container">
            <div class="el-gallery">

                <a data-fancybox="gallery" href="./images/elki_optom1.jpg" href="#" class="el-gallery__card">
                    <img src="./images/elki_optom1.jpg" alt="Галлерея">
                </a>

                <a data-fancybox="gallery" href="./images/elki_optom2.jpg" href="#" class="el-gallery__card">
                    <img src="./images/elki_optom2.jpg" alt="Галлерея">
                </a>

                <a data-fancybox="gallery" href="./images/elki_optom3.jpg" href="#" class="el-gallery__card">
                    <img src="./images/elki_optom3.jpg" alt="Галлерея">
                </a>

                <a data-fancybox="gallery" href="./images/elki_optom4.jpg" href="#" class="el-gallery__card">
                    <img src="./images/elki_optom4.jpg" alt="Галлерея">
                </a>

                <a data-fancybox="gallery" href="./images/elki_optom5.jpg" href="#" class="el-gallery__card">
                    <img src="./images/elki_optom5.jpg" alt="Галлерея">
                </a>

                <a data-fancybox="gallery" href="./images/elki_optom16.jpg" href="#" class="el-gallery__card">
                    <img src="./images/elki_optom16.jpg" alt="Галлерея">
                </a>

               

            </div>
        </div>

        <div class="container">
            <div class="el-videos">
                <div class="el-videos__title">Видео</div>
                <div class="row">
                    <div class="col-md-6">
                        <iframe width="100%" height="315" src="https://www.youtube.com/embed/XNfrsopih0w" frameborder="0" allowfullscreen></iframe>
                    </div>
                    <div class="col-md-6">
                        <iframe width="100%" height="315" src="https://www.youtube.com/embed/GH27yhkiFuw" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>

       <div class="el-seo">
            <div class="container">
                <div class="el-seo__title"><span>Оптовая продажа</span> искусственных новогодних елок без посредников</div>
                <article class="el-seo__article">
                    <p>
                    Подготовка к Новому году всегда начинается с покупки красивой и пышной елочки, которая становится главным украшением зимнего торжества. Накануне волшебного праздника спрос на искусственные елки оптом растет с каждым днем, и это неудивительно, ведь рукотворные деревца хорошо держат форму, не осыпаются, легко выдерживают вес гирлянд и стеклянных игрушек.
                    </p>
                    <p>
                    Если вы планируете приобрести <strong>искусственные елки оптом в Москве и других регионах России</strong>, наша компания рада предоставить вам прекрасный выбор деревьев, различающихся по высоте, диаметру и количеству веток. Пышные и нарядные, они почти не отличаются от настоящих, выглядят торжественно и очень реалистично.
                    </p>
                    <h3>Новогодние елки от прямого поставщика</h3>
                    <p>
                    Последние месяцы перед новогодними праздниками – это золотое время для предприятий малого и среднего бизнеса, планирующих получить дополнительный доход от своих капиталовложений. Решив <strong>купить новогодние елки оптом</strong>, можно обзавестись популярным товаром, который необходим практически каждой семье. Хорошо известно, что доходы «в сезон» часто приравниваются к прибыли, зарабатываемой в течение всего года, поэтому предстоящие торжества станут удачным моментом для краткосрочных и выгодных инвестиций.
                    </p>
                    <p>
                    ООО «Искусственные елки оптом» присутствует на рынке более 5 лет и специализируется на поставках новогодних красавиц в любой уголок страны. Мы предлагаем наиболее востребованные модели деревьев, которые легко продаются и приносят хороший доход. Заказывая у нас, вы получаете товар безупречного качества по приятной цене!
                    </p>
                    <p>
                    За время своей работы наша компания зарекомендовала себя как надежный партнер, гарантирующий заказчикам выгодные условия сотрудничества и множество значимых преимуществ:
                    </p>
                    <ul>
                        <li>Большие объемы на складе. Наши складские запасы включают в себя объемный и разнообразный ассортимент, удовлетворяющий запросам каждого покупателя.</li>
                        <li><strong>Доставка по всей России</strong>. Представительства компании работают в 17 российских регионах, благодаря чему ели доставляются клиенту в течение 2–3 дней с момента заказа.</li>
                        <li>Приемлемая стоимость. Мы предлагаем максимально доступные цены, обеспечивающие хорошую прибыль при розничных распродажах.</li>
                        <li>Работа <strong>без посредников</strong>. Наша компания работает напрямую с клиентом, что значительно сокращает логистическую цепочку и позволяет избежать излишних расходов.</li>
                        <li>Помощь в создании бизнес-плана. Дополнительным бонусом для желающих <strong>купить елку оптом в Москве</strong> становится бесплатный бизнес-план, который поможет запустить продажи деревьев всего за 2 дня. Успейте занять свою нишу и получить прибыль в сезон самых крупных праздничных распродаж!</li>
                    </ul>
                    <h3>Искусственные елки оптом – красиво и качественно!</h3>
                    <p>
                    <strong>Оптовая продажа новогодних елок</strong> – это замечательная возможность <strong>дешево</strong> приобрести качественный товар, который пользуется повышенным спросом накануне зимнего праздника. Предлагаемые деревья поставляются напрямую от производителя и представляют собой хорошо продаваемые позиции, выполненные из гипоаллергенных и экологически чистых материалов. Наши ели имеют праздничный вид, не нуждаются в особом уходе и отличаются массой бесспорных достоинств:
                    </p>
                    <ul>
                        <li>пышные ветки с длинными иголками от 5 до 7 см и снежным напылением, которое делает новогоднюю красавицу роскошной и по-настоящему неотразимой;</li>
                        <li>украшения в виде живых шишек, создающие атмосферу густого зимнего леса;</li>
                        <li>удобные подставки, обеспечивающие хорошую устойчивость деревца даже под большим весом игрушек;</li>
                        <li>прочные крепления, которые гарантируют простоту в сборке, разборке и хранении;</li>
                        <li>широкий спектр размеров – от небольших елочек до стройных красавиц высотой 240 см.</li>
                    </ul>
                    <p>
                    Решение <strong>купить ель на Новый год</strong> в нашей компании является лучшим выбором при ведении бизнеса и позволяет предпринимателям стать посредниками в создании волшебного праздника для каждой семьи!
                    </p>
                </article>
            </div>
        </div>

        <div class="el-footer">
            <iframe src="https://yandex.ru/map-widget/v1/-/CBUhuIHZWC" width="100%" height="610" frameborder="0"></iframe>
            <div class="el-footer__container">
                <h6 class="el-footer__title">ООО «ИСКУСCТВЕННЫЕ ЁЛКИ ОПТОМ»</h6>
                <div class="el-footer__row">
                    <div>
                        <img src="./images/icon_place.png" alt="Место">
                    </div>
                    <div>
                        <p>
                            Шоу-рум: г. Москва, Смоленский бульвар 17c5 <br>
                            Склад: г. Москва, Волгоградский проспект 42к5
                        </p>
                    </div>
                </div>
                <div class="el-footer__row">
                    <div>
                        <img src="./images/icon_doc.png" alt="Данные">
                    </div>
                    <div>
                        <p>
                            ОГРН 317502900052196; ИП Белый К.Ю. <br>
                        </p>
                    </div>
                </div>
                <div class="el-footer__row">
                    <div>
                        <img src="./images/icon_phone.png" alt="Телефон">
                    </div>
                    <div>
                        <a href="tel:+74996382991"><strong>8 (499) 638-29-91</strong></a><br>
                        <a href="tel:+79858503872">8 (985) 850-38-72</a>
                        <a href="#" class="el-footer__small-social">
                            <img src="./images/icon_wu.png" alt="WhatsUp">
                        </a>
                        <a href="#" class="el-footer__small-social">
                            <img src="./images/icon_tg.png" alt="Telegram">
                        </a>
                        <a href="#" class="el-footer__small-social">
                            <img src="./images/icon_vb.png" alt="Viber">
                        </a>
                    </div>
                </div>
                <div class="el-footer__row">
                    <div>
                        <img src="./images/icon_doc.png" alt="Данные">
                    </div>
                    <div>
                        <a href="#forma" class="el-footer__price" onClick="yaCounter46326543.reachGoal('order4');">Скачать прайс для оптовиков</a>
                    </div>
                </div>
                <!--<ul class="el-footer__social">
                    <li>
                        <a href="#"><img src="./images/icon_vk.png" alt="VK"></a>
                    </li>
                    <li>
                        <a href="#"><img src="./images/icon_fb.png" alt="FB"></a>
                    </li>
                    <li>
                        <a href="#"><img src="./images/icon_ig.png" alt="IG"></a>
                    </li>
                </ul>-->
                <div class="el-footer__gallery">
                    <img src="./images/footer_1.png" alt="Галлерея">
                    <img src="./images/footer_2.png" alt="Галлерея">
                </div><br>
                <a href="https://docs.google.com/document/d/1gjvlxni9PkFHBTEr9UV79VBCC4nMRq68_WoG1WA0hCA/edit?usp=sharing" target="_blank">Политика конфеденциальности</a><br>
                <a href="#">Партнерская программа</a>
            </div>
        </div>

        <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-scrollTo/2.1.0/jquery.scrollTo.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js" integrity="sha256-4Cr335oZDYg4Di3OwgUOyqSTri0jUm2+7Gf2kH3zp1I=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.js" integrity="sha256-MXT+AJD8HdXQ4nLEbqkMqW3wXXfvjaGQt/Q/iRlBNSU=" crossorigin="anonymous"></script>

        <script src="scripts/jquery.countdown.js"></script>
        <script src="scripts/main.js"></script>

        <script type="application/ld+json">
            {
                "@context": "http://schema.org",
                "@type": "Organization",
                "url": "http://www.example.com",
                "logo": "http://www.example.com/images/logo.png"
            }
        </script>

        <script type="text/javascript">
            jQuery(function() {
                jQuery('.this_is_my_lead_form').on('submit', function() {
                    var d = new Date();
                    var hours = d.getHours();
                    var minutes = d.getMinutes();
                    if (hours <= 9) hours = "0" + hours;
                    if (minutes <= 9) minutes = "0" + minutes;
                });
            });
        </script>

        <script type="text/javascript">
            $(function() {

                var current_date = new Date(); // текущая дата
                current_date.setDate(current_date.getDate())
                curDay = current_date.getDate(11),
                    curMonth = current_date.getMonth(),
                    curYear = current_date.getFullYear();


                $('#counter1').countMe(curYear, curMonth, curDay + 1, '#counter1');
                $('#counter2').countMe(curYear, curMonth, curDay + 1, '#counter2');
            });
        </script>

<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter46326543 = new Ya.Metrika({
                    id:46326543,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    trackHash:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/46326543" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<script>
(function(w, d, s, h, id) {
    w.roistatProjectId = id; w.roistatHost = h;
    var p = d.location.protocol == "https:" ? "https://" : "http://";
    var u = /^.*roistat_visit=[^;]+(.*)?$/.test(d.cookie) ? "/dist/module.js" : "/api/site/1.0/"+id+"/init";
    var js = d.createElement(s); js.async = 1; js.src = p+h+u; var js2 = d.getElementsByTagName(s)[0]; js2.parentNode.insertBefore(js, js2);
})(window, document, 'script', 'cloud.roistat.com', 'f119548a8e3f1d6dc285367471c4e967');
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-108301137-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-108301137-1');
</script>

<!-- Rating@Mail.ru counter -->
<script type="text/javascript">
var _tmr = window._tmr || (window._tmr = []);
_tmr.push({id: "2937196", type: "pageView", start: (new Date()).getTime()});
(function (d, w, id) {
  if (d.getElementById(id)) return;
  var ts = d.createElement("script"); ts.type = "text/javascript"; ts.async = true; ts.id = id;
  ts.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//top-fwz1.mail.ru/js/code.js";
  var f = function () {var s = d.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ts, s);};
  if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); }
})(document, window, "topmailru-code");
</script><noscript><div>
<img src="//top-fwz1.mail.ru/counter?id=2937196;js=na" style="border:0;position:absolute;left:-9999px;" alt="" />
</div></noscript>
<!-- //Rating@Mail.ru counter -->



<link rel="stylesheet" href="https://cdn.envybox.io/widget/cbk.css">
<script type="text/javascript" src="https://cdn.envybox.io/widget/cbk.js?wcb_code=bac8d90c7a0458fd7bacfd34f8bb108b" charset="UTF-8" async></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/inputmask/inputmask.min.js"></script>
    <script>
        $(document).ready(function() {
            $("#phone").mask("+7 (999) 999-99-99");
            $("#phone2").mask("+7 (999) 999-99-99");
            $("#email").inputmask("{1,20}@{1,20}.*{3}")
            $("#email2").inputmask("{1,20}@{1,20}.*{3}")
        });
    </script>


    </body>

    </html>